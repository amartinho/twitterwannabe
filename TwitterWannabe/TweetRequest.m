//
//  TweetRequest.m
//  TwitterWannabe
//
//  Created by Andre Martinho on 08/08/15.
//  Copyright (c) 2015 Martinho. All rights reserved.
//

#import "TweetRequest.h"
#import <TwitterKit/TwitterKit.h>

@interface TweetRequest(){
    NSMutableArray *tweetsArray;
    id<TweetRequestDelegate> delegate;
}
@end

@implementation TweetRequest

-(instancetype)initWithDelegate:(id<TweetRequestDelegate>)tweetDelegate {
    if(self = [super init]){
        [self clearTweets];
        delegate = tweetDelegate;
    }
    
    return self;
}

-(NSMutableArray*)getTweetsArray {
    return tweetsArray;
}

-(void)clearTweets{
    tweetsArray = [[NSMutableArray alloc]init];
}

-(void)reloadTweets {
    [self clearTweets];
    
    [self getTwentyRandomTweets];
}

/**
 *  I know it's not the most functional function but it's just to show some tweets
 */
-(void)getTwentyRandomTweets{
    [[Twitter sharedInstance] logInGuestWithCompletion:^(TWTRGuestSession *guestSession, NSError *error) {
        [[[Twitter sharedInstance] APIClient] loadTweetsWithIDs:[self getRandomTweets] completion:^(NSArray *tweets, NSError *error){
            
            if(delegate && [delegate respondsToSelector:@selector(finishedRequest)]){
                [[self getTweetsArray] addObjectsFromArray:tweets];
                [delegate finishedRequest];
            }
            
        }];
    }];
}

-(NSArray*)getRandomTweets{
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < 20; i++) {
        [array addObject:[self getRandomTweet]];
    }
    return [array copy];
}


-(NSString *)getRandomTweet{
    return [NSString stringWithFormat:@"%u", arc4random_uniform(INT_MAX)];
}


@end
