
//
//  Singleton.m
//  TwitterWannabe
//
//  Created by Andre Martinho on 08/08/15.
//  Copyright (c) 2015 Martinho. All rights reserved.
//

#import "Singleton.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <TwitterKit/TwitterKit.h>


@implementation Singleton


+(void)initializeSingletons{
    [Fabric with:@[CrashlyticsKit,TwitterKit]];
}



@end
