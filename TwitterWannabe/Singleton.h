//
//  Singleton.h
//  TwitterWannabe
//
//  Created by Andre Martinho on 08/08/15.
//  Copyright (c) 2015 Martinho. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Singleton : NSObject

/**
 *  Initialize our singletons
 */
+(void)initializeSingletons;

@end
