//
//  AppDelegate.h
//  TwitterWannabe
//
//  Created by Andre Martinho on 08/08/15.
//  Copyright (c) 2015 Martinho. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

