//
//  SearchTableViewController.m
//  TwitterWannabe
//
//  Created by Andre Martinho on 08/08/15.
//  Copyright (c) 2015 Martinho. All rights reserved.
//

#import "SearchTableViewController.h"
#import "TweetRequest.h"
#import "TweetModel.h"

@interface SearchTableViewController()<TweetRequestDelegate>{
    TweetRequest *tweetRequest;
}
@end

@implementation SearchTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle:NSLocalizedString(@"controller.search.title", @"Search Controller Title")];

    tweetRequest = [[TweetRequest alloc]initWithDelegate:self];
    
    [self loadXibs];
    
    [self setupTableView];
    
    [self setupRightButton];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self reloadTweets];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[tweetRequest getTweetsArray] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [TweetModel createCellUsing:[[tweetRequest getTweetsArray] objectAtIndex:[indexPath row]] forTableView:tableView withIndexPath:indexPath];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [[UIApplication sharedApplication] openURL:[[[tweetRequest getTweetsArray] objectAtIndex:[indexPath row]]permalink]];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [TweetModel tweetCellSize];
}

-(void)reloadTweets{
    [tweetRequest reloadTweets];
    [self.tableView reloadData];
}

#pragma mark - Load Xibs
-(void)loadXibs{
    UINib *tweetNib = [UINib nibWithNibName:@"TweetTableViewCell" bundle:nil];
    [self.tableView registerNib:tweetNib forCellReuseIdentifier:[TweetModel cellIdentifier]];
}

#pragma mark - TweetRequestDelegate
-(void)finishedRequest{
    [self.tableView reloadData];
}


#pragma mark - Setup Table
-(void)setupTableView{
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
}

#pragma mark - Setup Right Button
-(void)setupRightButton{
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(reloadTweets)];
    
    [self.navigationItem setRightBarButtonItem:refreshButton];
}


@end
