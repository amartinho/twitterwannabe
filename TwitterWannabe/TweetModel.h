//
//  TweetModel.h
//  TwitterWannabe
//
//  Created by Andre Martinho on 08/08/15.
//  Copyright (c) 2015 Martinho. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TWTRTweet.h"

@interface TweetModel : NSObject



/**
 *  Create the cell and populate it
 *
 *  @param tweet     TWTRTweet object used to populate the information of the cell
 *  @param tableView UITableView where we are going to populate the cell
 *  @param indexPath position of the cell we want to populate
 *
 *  @return UITableViewCell for the tableView
 */
+(UITableViewCell*)createCellUsing:(TWTRTweet*)tweet forTableView:(UITableView*)tableView withIndexPath:(NSIndexPath*)indexPath;


/**
 *  Cell size
 *
 *  @return CGFloat with the size
 */
+(CGFloat)tweetCellSize;

/**
 *  Cell identifier
 *
 *  @return NSString with the cell identifier
 */
+(NSString*)cellIdentifier;

@end
