//
//  TweetModel.m
//  TwitterWannabe
//
//  Created by Andre Martinho on 08/08/15.
//  Copyright (c) 2015 Martinho. All rights reserved.
//

#import "TweetModel.h"
#import "TweetTableViewCell.h"

@implementation TweetModel


+(UITableViewCell*)createCellUsing:(TWTRTweet*)tweet forTableView:(UITableView*)tableView withIndexPath:(NSIndexPath*)indexPath{
    
    TweetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[self cellIdentifier] forIndexPath:indexPath];
    
    [cell populateCellUsing:tweet];
        
    return cell;
}

+(CGFloat)tweetCellSize {
    return [TweetTableViewCell cellSize];
}

+(NSString*)cellIdentifier {
	return @"tweetIdentifier";
}
@end
