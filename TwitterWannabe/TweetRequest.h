//
//  TweetRequest.h
//  TwitterWannabe
//
//  Created by Andre Martinho on 08/08/15.
//  Copyright (c) 2015 Martinho. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TweetRequestDelegate<NSObject>

-(void)finishedRequest;

@end


@interface TweetRequest : NSObject

/**
 *  Initialize TweetRequest Object with a TweetRequestDelegate
 *
 *  @return TweetRequest Instance
 */
-(instancetype)initWithDelegate:(id<TweetRequestDelegate>)tweetDelegate;

/**
 *  Get the TweetsArray
 *
 *  @return NSMutableArray with a number of TWTRTweet objects
 */
-(NSMutableArray*)getTweetsArray;

/**
 *  Delete previous tweets and get 20 more random ones
 */
-(void)reloadTweets;

@end
