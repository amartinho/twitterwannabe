//
//  TweetTableViewCell.h
//  TwitterWannabe
//
//  Created by Andre Martinho on 08/08/15.
//  Copyright (c) 2015 Martinho. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TWTRTweet.h"

@interface TweetTableViewCell : UITableViewCell

/**
 *  Populate the cell with the TWTRTweet object
 *
 *  @param tweet TWTRTweet Object with all the informations from the tweet
 */
-(void)populateCellUsing:(TWTRTweet *)tweet;


/**
 *  Cell size for the TableViewCell
 *
 *  @return CGFloat with the size of the Cel
 */
+(CGFloat)cellSize;

@end
