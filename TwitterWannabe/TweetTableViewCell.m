//
//  TweetTableViewCell.m
//  TwitterWannabe
//
//  Created by Andre Martinho on 08/08/15.
//  Copyright (c) 2015 Martinho. All rights reserved.
//

#import "TweetTableViewCell.h"
#import "TWTRUser.h"
#import "UIImageView+AFNetworking.h"

@interface TweetTableViewCell(){
    
    __weak IBOutlet UIImageView *tweetImageView;
    __weak IBOutlet UILabel *tweetTextLabel;
    __weak IBOutlet UILabel *userTextLabel;
    __weak IBOutlet UILabel *favouritesTextLabel;
    __weak IBOutlet UILabel *retweetedTextLabel;
}

@end

@implementation TweetTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)populateCellUsing:(TWTRTweet *)tweet {
 
    [tweetTextLabel setText:[tweet text]];
    [retweetedTextLabel setText:[NSString stringWithFormat:@"RT:%lld",[tweet retweetCount]]];
    [favouritesTextLabel setText:[NSString stringWithFormat:@"FV:%lld",[tweet favoriteCount]]];
    [userTextLabel setText:[[tweet author] formattedScreenName]];
    
    //Image Loading
    [tweetImageView setImageWithURLRequest: [[NSURLRequest alloc]initWithURL:[[NSURL alloc] initWithString:[[tweet author] profileImageURL]]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        if(image){
            [tweetImageView setImage:image];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        NSLog(@"Error downloading image = %@", [error localizedDescription]);
    }];
  
}

+(CGFloat)cellSize {
    return 100;
}

@end
